
public class HomeWork1 {

    public String intToKop(int kop) {
        String result;
        if (kop%10 == 0) {
            result = "копеек";
        } else if (kop%10 == 1) {
            result = "копейка";
        } else {
            result = "копейки";
        }
        return result;
    }

    public String intToMonth(int month) {
        String monthString;
        switch (month) {
            case 1:  monthString = "January";
                break;
            case 2:  monthString = "February";
                break;
            default: monthString = "Invalid month";
                break;
        }
        System.out.println(monthString);
        return monthString;
    }

}
